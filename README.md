Pay VC project summary
===================

### We aim to provide one essential piece that is missing for uncommitted parties to engage in SSI with enthusiasm: the ability to get paid for sharing valuable insights (verifiable credential) will greatly encourage SSI uptake. 

### Overview

We believe three components are necessary and sufficient. 

- Issuers get the means to specify the terms and conditions for sale and verify that a payment has been executed. 

- Holders get the means to pay using “programmable money,” drawing on a prepay system, or tapping into an escrow account. More commonly, the holder will be able to forward the bill to a verifier. 

- Verifiers, typically a service provider seeking business from qualified holders, get the means to deal with the invoice or draw on a partner to facilitate the payment. 

To protect privacy, we propose to link payments to the act of issuing (as opposed to presenting or verifying VCs). As a minimum, the logic would need to support most both debit and credit payments, the ability to charge different prices based on urgency and expiry, “invoicing” compliant with account and vat rules, and possibly to deal with refunds and the assistance of third parties to settle a transaction on behalf of all parties involved. 

![](https://i.imgur.com/qpBSGvn.png)
